# Lung Segmentation Model

## Steps for testing the lung segmentation model

1. Open the folder `lung-segmentation`.
2. Open the `main.py` file in this folder.
3. Change the input image path in the *image_path* variable.
4. Save the changes by pressing `Ctrl + S` and close the file.
5. Open terminal in this directory ("classification-code") and type `python main.py` or `python3 main.py` and press Enter key.
6. The lung segmented output would be saved in `output.png` in the same directory "classification-code".
