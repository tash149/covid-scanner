import cv2
import matplotlib.pyplot as plt
from keras.preprocessing.image import img_to_array, load_img

img_path = "lungVAE/pred_20200722_11_10/person90_virus_169.jpeg"
image = cv2.imread(img_path)

image = cv2.resize(image, (256, 256))
image = img_to_array(image)
print(image.shape)
plt.imshow(image)
plt.savefig("trial.jpg")
