from keras.models import model_from_json
import cv2
import numpy as np
import matplotlib.pyplot as plt

# Loading the pretrained Lung Segmentation model
json_file = open('../trained-models/v_model_v2.json', 'r') # model json file path
model_json = json_file.read()
json_file.close()
vnet_model = model_from_json(model_json)
# load weights into new model
vnet_model.load_weights("../trained-models/v_model_v2.h5") # model .h5 file path


inputs=[] # For inputting the image and storing it into numpy format
image_path = '../test-data/ar.png' # Write the path to the input chest xray image from "test-data" folder example('../test-data/xray.jpg')
im1 = cv2.resize(cv2.imread(image_path), (256,256))  # Reading the input image and resizing it
inputs.append(im1.reshape([256,256,3]))
X = np.stack(inputs, axis=0)
X_ = X/255.0

new_results = vnet_model.predict(X_)
crop=[] # for storing the cropped images

x = new_results.reshape((256,256)) # assert shape
y_ = np.where(x > 0.5, 1, 0) # true mask
y_ = y_.astype('uint8') #make sure the input image is also in this format
crop.append(cv2.bitwise_and(X_[0],X_[0],mask=y_))
plt.imshow(crop[0])

plt.imsave('output.png',crop[0]) #saving image to the local directory