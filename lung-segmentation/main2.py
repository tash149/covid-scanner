print("yolaaaaaa")
import cv2
import keras
import numpy as np
from keras.models import Model, load_model
from keras.models import model_from_json
from keras.optimizers import Adam
from keras import backend as K
import torch
import torch.nn as nn
import glob
import time
import os

torch.manual_seed(42)
np.random.seed(42)
from lungVAE.models.VAE import uVAE
import torchvision.transforms.functional as TF
from torchvision.utils import save_image


from pydicom import dcmread
from skimage.transform import resize
import matplotlib.pyplot as plt
import torchvision.transforms.functional as TF
from skimage.exposure import equalize_hist as equalize
from skimage.io import imread, imsave
from skimage import img_as_ubyte
from skimage.color import rgb2gray
from lungVAE.utils.postProcess import postProcess
from lungVAE.utils.tools import dice, binary_accuracy
from torchvision.utils import save_image
from keras.preprocessing.image import img_to_array, load_img

g_model = load_model("../trained-models/g_model_w_loss")


def loadDCM(f, no_preprocess=False, dicom=False):
    wLoc = 448
    ### Load input dicom
    if dicom:
        dcmFile = dcmread(f)
        dcm = dcmFile.pixel_array
        dcm = dcm / dcm.max()
        if dcmFile.PhotometricInterpretation == "MONOCHROME1":
            ### https://dicom.innolitics.com/ciods/ct-image/image-pixel/00280004 ###
            ### When MONOCHROME1, 0->bright, 1->dark intensities
            dcm = 1 - dcm
    else:
        ## Load input image
        dcm = imread(f)
        dcm = dcm / dcm.max()
    if not no_preprocess:
        dcm = equalize(dcm)

    if len(dcm.shape) > 2:
        dcm = rgb2gray(dcm[:, :, :3])

    ### Crop and resize image to 640x512
    hLoc = int((dcm.shape[0] / (dcm.shape[1] / wLoc)))
    if hLoc > 576:
        hLoc = 576
        wLoc = int((dcm.shape[1] / (dcm.shape[0] / hLoc)))

    img = resize(dcm, (hLoc, wLoc))
    img = torch.Tensor(img)
    pImg = torch.zeros((640, 512))
    h = (int((576 - hLoc) / 2)) + p
    w = int((448 - wLoc) / 2) + p
    roi = torch.zeros(pImg.shape)
    if w == p:
        pImg[np.abs(h) : (h + img.shape[0]), p:-p] = img
        roi[np.abs(h) : (h + img.shape[0]), p:-p] = 1.0
    else:
        pImg[p:-p, np.abs(w) : (w + img.shape[1])] = img
        roi[p:-p, np.abs(w) : (w + img.shape[1])] = 1.0

    imH = dcm.shape[0]
    imW = dcm.shape[1]
    pImg = pImg.unsqueeze(0).unsqueeze(0)
    return pImg, roi, h, w, hLoc, wLoc, imH, imW


def saveMask(f, img, h, w, hLoc, wLoc, imH, imgW, no_post=False):

    img = img.data.numpy()
    imgIp = img.copy()

    if w == p:
        img = resize(img[np.abs(h) : (h + hLoc), p:-p], (imH, imW), preserve_range=True)
    else:
        img = resize(img[p:-p, np.abs(w) : (w + wLoc)], (imH, imW), preserve_range=True)
    imsave(f, img_as_ubyte(img > 0.5))
    # plt.imshow(img_as_ubyte(img > 0.5))
    # plt.savefig("trial.jpg")
    # print(img.shape)

    if not no_post:
        imgPost = postProcess(imgIp)
        if w == p:
            imgPost = resize(imgPost[np.abs(h) : (h + hLoc), p:-p], (imH, imW))
        else:
            imgPost = resize(imgPost[p:-p, np.abs(w) : (w + wLoc)], (imH, imW))

        imsave(f.replace(".png", "_post.png"), img_as_ubyte(imgPost > 0.5))


lm2_path = "lungVAE/saved_models/lungVAE.pt"
p = 32
device = torch.device("cpu")
net = uVAE(nhid=16, nlatent=8, unet=False)
net.load_state_dict(torch.load(lm2_path, map_location=device))
net.to(device)
t = time.strftime("%Y%m%d_%H_%M")
save_dir = "lungVAE/pred_" + t + "/"
if not os.path.exists(save_dir):
    os.mkdir(save_dir)


filetype = "png"
files = glob.glob("lungVAE/dataset/*")
files = sorted(files)
# print(files)
# print(len(files))

# Image Preprocessing
def transform_img_fn(path_list):
    out = []
    for img_path in path_list:
        image = cv2.imread(img_path)
        image = cv2.resize(image, (256, 256))
        image = img_to_array(image)
        out.append(image)
    return out


img = transform_img_fn(files)
data = np.array(img, dtype="float") / 255.0
bone_result = g_model.predict(data)


for fIdx in range(len(files)):
    f = files[fIdx]
    fName = f.split("/")[-1]
    img, roi, h, w, hLoc, wLoc, imH, imW = loadDCM(f, no_preprocess=False, dicom=False)

    img = img.to(device)
    _, mask = net(img)
    mask = torch.sigmoid(mask * roi)
    f = save_dir + fName.replace("." + filetype, "_mask.png")

    x = np.arange(10)
    y = np.arange(10)
    plt.figure(figsize=(30, 30))
    plt.xticks(x, "")
    plt.yticks(y, "")
    plt.imshow(bone_result[fIdx])
    plt.savefig(
        "lungVAE/bs-images/"
        + str(fName.split(".")[0])
        + "_bs-output-"
        + str(fIdx)
        + ".jpg",
        bbox_inches="tight",
    )

    saveMask(f, mask.squeeze(), h, w, hLoc, wLoc, imH, imW, False)
    print("Segmenting %d/%d" % (fIdx, len(files)))

mask_files = glob.glob(save_dir + "*")

files = sorted(files)
mask_files = sorted(mask_files)

# print(len(mask_files))
print(mask_files)
print(files)

mask_imgs = transform_img_fn(mask_files)

kernel = np.ones((25, 25), np.uint8)  # kernel size
crop = []  # This will contain all the segmented images of shape (256,256,3)

for i in range(len(mask_files)):
    # y_ = np.where(mask_imgs[i] > 0.5, 1, 0)  # true mask
    # y_ = y_.astype("uint8")
    # dilation = cv2.dilate(y_,kernel,iterations = 1)

    # print(bone_result[0].shape)
    # print(y_.shape)
    fname = mask_files[i].split("/")[-1]
    print(mask_imgs[i].shape)
    print(bone_result[i].shape)

    plt.imshow(mask_imgs[i])
    plt.savefig("trial" + str(i) + ".jpg")

    # mask_img[i] = np.where(mask_img[i] > 0.8, 1, 0)  # true mask
    # plt.imshow(mask_img[i])
    # plt.savefig("trial" + str(i) + ".jpg")

    gray_img = cv2.cvtColor(mask_imgs[i], cv2.COLOR_BGR2GRAY)
    gray_img = np.where(gray_img > 0.8, 1, 0)  # true mask
    gray_img = gray_img.astype("uint8")

    # plt.imshow(gray_img)
    # plt.savefig("trial" + str(i) + ".jpg")

    print(gray_img)
    crop.append(cv2.bitwise_and(bone_result[i], bone_result[i], mask=gray_img))
    x = np.arange(10)
    y = np.arange(10)
    plt.figure(figsize=(30, 30))
    plt.xticks(x, "")
    plt.yticks(y, "")
    plt.imshow(crop[i])
    plt.savefig(
        "lungVAE/lung-segment/" + fname + "_lung-segment.jpg", bbox_inches="tight",
    )


"""""" """""" """""" """""" """""" """""" """""" """""" """
""" """""" """""" """""" """""" """""" """""" """""" """"""
"""
from keras.models import model_from_json
import cv2
import numpy as np
import matplotlib.pyplot as plt

# Loading the pretrained Lung Segmentation model
json_file = open("../trained-models/v_model_v2.json", "r")  # model json file path
model_json = json_file.read()
json_file.close()
vnet_model = model_from_json(model_json)
# load weights into new model
vnet_model.load_weights("../trained-models/v_model_v2.h5")  # model .h5 file path


inputs = []  # For inputting the image and storing it into numpy format
image_path = "../test-data/ar.png"  # Write the path to the input chest xray image from "test-data" folder example('../test-data/xray.jpg')
im1 = cv2.resize(
    cv2.imread(image_path), (256, 256)
)  # Reading the input image and resizing it
inputs.append(im1.reshape([256, 256, 3]))
X = np.stack(inputs, axis=0)
X_ = X / 255.0

new_results = vnet_model.predict(X_)
crop = []  # for storing the cropped images

x = new_results.reshape((256, 256))  # assert shape
y_ = np.where(x > 0.5, 1, 0)  # true mask
y_ = y_.astype("uint8")  # make sure the input image is also in this format
crop.append(cv2.bitwise_and(X_[0], X_[0], mask=y_))
plt.imshow(crop[0])

plt.imsave("output.png", crop[0])  # saving image to the local directory

























f = files[fIdx]
    fName = f.split("/")[-1]
    img, roi, h, w, hLoc, wLoc, imH, imW = loadDCM(f, no_preprocess=False, dicom=False)

    img = img.to(device)
    _, mask = net(img)
    mask = torch.sigmoid(mask * roi)
    f = save_dir + fName.replace("." + filetype, "_mask.png")
    print(mask.shape)
    x = np.arange(10)
    y = np.arange(10)
    plt.figure(figsize=(30, 30))
    plt.xticks(x, "")
    plt.yticks(y, "")
    plt.imshow(bone_result[fIdx])
    plt.savefig(
        "lungVAE/bs-images/"
        + str(fName.split(".")[0])
        + "_bs-output-"
        + str(fIdx)
        + ".jpg",
        bbox_inches="tight",
    )

    saveMask(f, mask.squeeze(), h, w, hLoc, wLoc, imH, imW, False)
    mask_np = mask.squeeze()
    print(mask_np.shape)
    mask_np = mask_np.data.numpy()
    imgIp = mask_np.copy()

    if w == p:
        mask_np = resize(
            256, 256
        )  # mask_np[np.abs(h) : (h + hLoc), p:-p], (imH, imW), preserve_range=True
    else:
        mask_np = resize(
            256,
            256
            
        )# mask_np[p:-p, np.abs(w) : (w + wLoc)], (imH, imW), preserve_range=True

    print(mask_np.shape)
    # pint(img_as_ubyte(mask_np > 0.5))
    mask_np = mask_np.astype("uint8")
    print(mask_np.shape)
    print(mask_np)

    crop.append(cv2.bitwise_and(bone_result[fIdx], bone_result[fIdx], mask=mask_np))
    x = np.arange(10)
    y = np.arange(10)
    plt.figure(figsize=(30, 30))
    plt.xticks(x, "")
    plt.yticks(y, "")
    plt.imshow(crop[fIdx])
    plt.savefig(
        "lungVAE/lung-segment/" + fName + "_lung-segment.jpg", bbox_inches="tight",
    )
    print("Segmenting %d/%d" % (fIdx, len(files)))



"""
