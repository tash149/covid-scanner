import glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms.functional as TF
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import TensorDataset, DataLoader, Dataset
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

from skimage.exposure import equalize_hist as equalize
from skimage.draw import random_shapes
from skimage.filters import gaussian


mask = sorted(glob.glob("masks/*.png"))
data = [f.split("/")[-1].replace("_mask.png", ".png") for f in mask]

N = len(mask)
"""
images = torch.zeros((N, 1, 256, 256))
labels = torch.zeros((N, 1, 256, 256))

for index in range(N):
    # image = Image.open(self.data_dir + "raw/equalized/" + data[index])
    label = Image.open(mask[index])
    print(mask[index])
    # h = int(image.height / (image.width / self.w))
    # if h > sel:
    #    self.w = int(image.width / (image.height / self.h))
    # image, label = (
    #    TF.resize(image, (self.h, self.w)),
    #    TF.resize(label, (self.h, self.w)),
    # )
    label = TF.to_tensor(label)
    print(label.shape)
    # image, label = pad(image), pad(label)
    labels[index] = label
print(labels.shape)
torch.save(labels, "lungData.pt")
"""


def make_masks(h, w, s, N, blur=False):
    masks = np.zeros((N, h, w))
    for i in range(N):
        skMask = (
            random_shapes(
                (h, w),
                min_shapes=10,
                max_shapes=20,
                min_size=s,
                allow_overlap=True,
                multichannel=False,
                shape="circle",
            )[0]
            < 128
        ).astype(float)
        if blur:
            skMask = gaussian(skMask, sigma=s / 2)  # 0.8 is ~dense opacity
        masks[i, 0] = skMask

    # pdb.set_trace()
    if blur:
        masks = torch.Tensor(masks)
    else:
        masks = torch.Tensor(masks).type(torch.BoolTensor)
        masks2 = np.array(np.uint8(masks))
        # masks = torch.Tensor(masks, dtype=bool)
    return masks


masks = make_masks(256, 256, 0, int(2 * N))
print("yooooo")
print(masks.shape)
plt.imshow(masks[0])

