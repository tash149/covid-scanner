# Chest-Radiograpghy-Disease-Prediction

## Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
  * [Running the software online](#running-the-software-online)
  *  [Running the software locally](#running-the-software-locally)
* [Code Run Through](#code-run-through)
* [Structural Outline](#structural-outline)
* [Contact](#contact)


## About The Project

Being in midst of a pandemic gave us a necessity to make an end to end software for the ease of pre-testing a patient.
* Classifying and differentiating between various diseases and a healthy individual:
	* Covid19
	* Non Covid-Viral Pneumonia 
	* Bacterial Pneumonia
	* Normal
* Ease of screening a patient

##  Getting Started

### Running the software online

**Steps :**
1. Open the link `[http://3.12.155.103/]`.
2. Login with the username and password as `admin`.
3. Click on `Choose File` and choose your desired chest x-ray image.
4. Crop the undesired parts of the images and click on `Crop and Upload`.
5. Click on `Upload and Test` button.
6. You will be able to view the outputs for the image at all the stages:
	* Bone Suppression
	* Segmentation
	* GradCAM output

### Running the software locally

### Steps for Ubuntu Operating System

**Prerequisites :**
1. An Ubuntu operating system.
2. Having `python 3.6` installed on the system, If not run the following commands on your terminal for Ubuntu 16.04+ :
	* `sudo apt-get update`
	* `sudo apt-get install python3.6`
3. The following python packages must be installed on your system:
	 * Flask  v1.1.2
	 * Keras v2.2.4
	 * Werkzeug  v1.0.0
	 * matplotlib  v3.0.2
	 * numpy v1.18.1
	 * opencv_contrib_python v4.2.0.34
	 * scikit_image v0.16.2
	 * tensorflow v1.14.0
	 * itsdangerous v1.1.0
	 * Jinja2 v2.10.1
	 * gunicorn v20.0.4 
5. The main folder provided to you on your system locally.
 
 **Steps :**
 1. Get inside the `website-code` folder using your bash terminal
 2. If you have `pip3` installed on your system you may skip this step, If not run the command `sudo apt-get install python3-pip` from your terminal
 3.  Next run the command `pip3 install -r requirements.txt` from your terminal
 4. If all the packages are installed you can proceed, else try running `pip3 install <package_name>` from the terminal, here <package_name> is the package that the system was unable to install for further installation details follow step 5.
 5. Run the following commands from your terminal one by one if the packages aren't yet installed:
	 *  pip3 install Flask==1.1.2
	 *  pip3 install Keras==2.2.4
	 *  pip3 install Werkzeug==1.0.0
	 *  pip3 install matplotlib==3.0.2
	 *  pip3 install numpy==1.18.1
	 *  pip3 install opencv-contrib-python==4.2.0.34
	 *  pip3 install scikit-image==0.16.2
	 *  pip3 install tensorflow==1.14.0
	 *  pip3 install itsdangerous==1.1.0
	 *  pip3 install Jinja2==2.10.1
	 *  pip3 install gunicorn==20.0.4
 6. Next from the same directory `website-code` run the command `flask run` 
 7. Voilà the web server would be up and running on your localhost `http://127.0.0.1:5000/`


## Code Run Through

The web application goes through the following steps to complete its functionalities:
1. Once the user hits the enter key on `flask run` the code in `app.py` starts to run and renders the first page of the url http://127.0.0.1:5000/ i.e. the login page (`index.html`).
2. The user is now prompted to ener the username and the password which is by default set to `admin`.
3. Once the user enters the correct credentials, the page redirects to `predict.html` that displays the main page of the website, here one can choose a file and upload it to the server for testing.
4. Then we click on the choose file button, which in the code is defined in `predict.html` with the input type as file.
5. Here after choosing the file the user is prompted to crop the image, for removing undesired parts of the image which is invoked from `predict.html` on method made in `croppie.js` .
6. Once the image is cropped and the user presses the `Crop & Upload Image` button, the cropped image is provided to the backend i.e `app.py` from `prediction_newv2.js` in base64 fromat.
7. Now in the `app.py` the base64 string is decoded back into the image which is further converted to a numpy array for the ease of processing.
8. This numpy array is passed through the pre-trained Bone Suppression model which returns the images with suppressed rib cage and collar bones.
9. The bone suppressed image is now passed into the pre-trained Lung Segmentation model which segments out the lungs and removes all the unnecessary parts of the chest x-ray.
10. The generated image is passed into the final pre-trained Classification model which classifies the image into 4 classes based on the features that it has extracted from the dataset, these classes are namely:
	* Covid19
	* Non Covid-Viral Pneumonia
	* Bacterial Pneumonia
	* Normal
11. Finally we plot Gradient-Weighted Class Activation Mapping (GradCAM) onto the cropped image from the last convolution layer and the second last dense layer. 
12. These are then returned back to frontend i.e `predict.html` from `app.py` in base64 format and are displayed on the page. 

## Structural Outline

![flowchart](https://user-images.githubusercontent.com/39271055/85957860-85e44880-b9ae-11ea-98c6-1e2174973d14.png)
