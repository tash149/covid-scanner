import cv2
import keras
import numpy as np
from keras.models import Model, load_model
from keras.models import model_from_json
from keras.optimizers import Adam
from keras import backend as K


inputs = []  # For inputting the image and storing it into numpy format
image_path = "../test-data/ar.png"  # Write the path to the input chest xray image from "test-data" folder example('../test-data/xray.jpg')

img = cv2.imread(image_path)  # Reading the image
img = cv2.resize(img, (256, 256))  # Resizing it
inputs.append(img.reshape([256, 256, 3]))  # Appending inputs
X1 = (
    np.array(inputs, dtype="float") / 255.0
)  # Preprocessing the image and scaling it to make it from (0-1)

# Loading the Bone Suppression model and applying it on the image
g_model = load_model("../trained-models/g_model_w_loss")
results = g_model.predict(X1)

# Model Architecture
loaded_model = keras.models.Sequential(
    [
        keras.layers.Conv2D(
            32, (3, 3), strides=(1, 1), padding="same", input_shape=(256, 256, 3)
        ),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(64, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(128, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(256, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(512, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(1024, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(1024, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(512, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(256, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Flatten(),
        keras.layers.Dense(1024),
        keras.layers.BatchNormalization(),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(512),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(256),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(128),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(64),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(32),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dense(4, activation="softmax"),
    ]
)

# Loss for Classification model
def focal_loss(y_true, y_pred):
    gamma = 2.0
    alpha = 0.25
    # Define epsilon so that the backpropagation will not result in NaN
    # for 0 divisor case
    epsilon = K.epsilon()
    # Add the epsilon to prediction value
    # y_pred = y_pred + epsilon
    # Clip the prediction value
    y_pred = K.clip(y_pred, epsilon, 1.0 - epsilon)
    # Calculate cross entropy
    cross_entropy = -y_true * K.log(y_pred)
    # Calculate weight that consists of  modulating factor and weighting factor
    weight = alpha * y_true * K.pow((1 - y_pred), gamma)
    # Calculate focal loss
    loss = weight * cross_entropy
    # Sum the losses in mini_batch
    loss = K.sum(loss, axis=1)
    return loss


# Compiling and loading model
loaded_model.compile(
    optimizer=Adam(), loss=focal_loss, metrics=["categorical_accuracy"]
)

loaded_model.load_weights("../trained-models/focal3_crop_more_imgs")


# Writing the final output into "output.txt"
final = loaded_model.predict(results)

file = open("output.txt", "w")

if np.argmax(final) == 0:
    file.write(image_path[13:] + " output: " + "Covid19")
if np.argmax(final) == 1:
    file.write(image_path[13:] + " output: " + "Non Covid/Viral Pneumonia")
if np.argmax(final) == 2:
    file.write(image_path[13:] + " output: " + "Bacterial Pneumonia")
if np.argmax(final) == 3:
    file.write(image_path[13:] + " output: " + "Normal")

file.close()
