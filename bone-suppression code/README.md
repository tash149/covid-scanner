# Bone Suppression

## Steps for testing the bone-suppression model

1. Open the folder `bone-suppression code`.
2. Open the `run.py` file
3. For the 'image_path' variable, add the path to your testing image, you can take any image from `test-data` folder.
4. Save the changes by pressing `Ctrl+S` and close the file.
5. Open the terminal in that directory ("bone-suppression code") and type: `python run.py` or `python3 run.py` and press Enter.
6. Resultant image will be saved in that directory for your view in `output.png`.
