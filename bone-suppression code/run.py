import keras
from keras.models import load_model
import os
from model import differential_model
import cv2
import matplotlib.pyplot as plt
import numpy as np

pre_trained_model = "../trained-models/g_model_w_loss" #write path to the pre_trained model, example - '../model/g_model_w_loss'
image_path = "../test-data/ar.png" #write the path to the chest xray image, you choose to suppress, example - '../test-data/xray.jpg'
ext = image_path.split(".")
model = load_model(pre_trained_model) #load the model
load_image = cv2.resize(cv2.imread(image_path), (256,256)) #load and resize image
load_image = load_image/255
if len(load_image.shape)==2: #check for channels
	load_image = cv2.cvtColor(load_image, cv2.COLOR_GRAY2BGR)
output_image = model.predict(load_image.reshape([1,256,256,3])) #get prediction
output_image = output_image*255
cv2.imwrite('output.png',output_image.reshape([256, 256, 3]))
