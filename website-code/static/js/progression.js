$(document).ready(function () {
    // Init
    $('.image-section1').hide();
    $('.image-section2').hide();
    $('.loader').hide();
    //$('#result').hide();

    // Upload Preview
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //$('#imagePreview1').css('background-image', 'url(' + e.target.result + ')');
                document.getElementById('imgpreview1').setAttribute('src', e.target.result);
                //$('#imagePreview1').hide();
                //$('#imagePreview1').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imgpreview2').setAttribute('src', e.target.result);
                //$('#imagePreview2').css('background-image', 'url(' + e.target.result + ')');
                //$('#imagePreview2').hide();
                //$('#imagePreview2').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imageUpload1").change(function () {
        $('.image-section1').show();
        //$('#btn-predict').show();
        //$('#result').text('');
        //$('#result').hide();
        readURL1(this);
    });

    $("#imageUpload2").change(function () {
        $('.image-section2').show();
        $('#btn-predict').show();
        //$('#result').text('');
        //$('#result').hide();
        readURL2(this);
    });

    // Predict
    $('#btn-predict').click(function () {
        var form_data = new FormData($('#upload-file')[0]);

        // Show loading animation
        $(this).hide(600);
        $('.loader').show(600);
        $('.block').hide(600);
        $('#btn-predict').hide(600);

        // Make prediction by calling api /predict
        $.ajax({
            type: 'POST',
            url: '/progressapi',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                // Get and display the result

                $('#result').html(`Result: ` + data);

                /*
                var analysis_result = data.substr(data.length - 1);
                var base64Stringimage    = data.substring(0, data.length - 1); 


                if(analysis_result == '0'){   
                    $('#result').html(' Result: No Viral class/Negative');
                    $('#result').addClass("result_ok");
                
                }
                if(analysis_result == '1'){
                    $('#result').html(' Result: Pneumonia/Posititve');
                    $('#result').addClass("result_pneumonia");
                    

                }
                if(analysis_result == '2'){
                    $('#result').html(' Result: Covid-19/Posititve');
                    $('#result').addClass("result_covid");
                    
                }
*/
                $('.patient_enter_data').hide(600);
                $('#result').show(600);
                //  $(".img-preview").css("width", "512px");
                // $(".img-preview").append(`<div style="background-image:url('data:image/png;base64,` + base64Stringimage + `'); min-width:256px;">
                //</div>`);
                $("#upload-file").prepend(`<span style="font-size: 120%;"><b>Patient ID:` + $("#patient_id").val() + `</b></span>`);
                $('.loader').hide(600);
                $('#btn-reload').show(600);

                console.log("Ajax Success Fired and Exiting Scope");
            },
        });
    });

});

function dummyajax(data) {

    $('#result').html(data);
    $('#result').show(600);

    $("#upload-file").prepend("Patient ID: " + $("#patient_id").val());
    $('.loader').hide(600);
    $('#btn-reload').show(600);

    console.log("Ajax Success Fired and Exiting Scope");
}