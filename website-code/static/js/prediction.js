
/**
 * Execute JS when HTML DOM is Ready. Wrapper for all JS Code.
 * @param  {Function} Pass function name or define the function to be launched when DOM is ready.
 * @return {Void} 
 */

$(document).ready(function () {

    if (window.innerWidth <= 850)
        $("#sample_sidebar").hide();


    $('.image-section').hide(); //Hide The Image(s) Container 
    $('.loader').hide();        //Hide The Loader (GIF)



    //Initialize the crop tool with 'croppie' and store into a variable.
    $image_crop = $('#image_demo').croppie({
        enableExif: true, //read the image Exif data and read the orientattion
        viewport: { width: 300, height: 300, type: 'square' }, //Defined the cropping area. type: [square, circle]
        boundary: { width: 350, height: 350 },
        showZoomer: true, //Slider for image zoom. If hidden you can use mouseWheelZoom.
        enableZoom: true, //Can image be zoomed?
        enableResize: true, //Can image be resized?
        enableOrientation: true, //Can image orientattion be changed?
        mouseWheelZoom: 'ctrl' //Bind key to be used for zoom with mousewheel.
    });



    //Event listner for file input. When the state chnaged. You add a file or a new file. This will be fired.
    $('#imageUpload').on('change', function () {
        var reader = new FileReader(); //Create file reading object so the file can be passed to the cropping tool. Native to browsers. 
        reader.onload = function (event) { //event is the actually the file. When the reader is intialized. pass the file to the 'croppie' tool. 
            document.getElementById('original_image').setAttribute('src', event.target.result);// Storing the original image to a element
            $image_crop.croppie('bind', {
                url: event.target.result //Bind the reader image with croppie.
            }).then(function () { //Logging to the Browser console. 
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]); //Read the file as URL converts, the file (image) to base64. This URL was binded with croppie
        // in the last step.
        $('#overlay').show(200); //Turns on black Transparent Opacity(0.5) Overlay background. 
        $('#ImageCropContainer').show(200); //Show the image crop container. 
    });




	/**
	* Crops the selected part of the image. Event listner. 
	* @param  {Function}
	*/

    $('.crop_image').click(function (event) {
        $image_crop.croppie('result', {
            type: 'base64', //Cropped image return type should be base64. Can be: [base64, canvas, blob]
            size: 'viewport' //Cropped image area. variable defined during intialization. 
        }).then(function (response) { // @response: cropped image returned in base64. 
            document.getElementById('image1').setAttribute('src', response); //Set the first image source as the response.
            $('.image-section').show(); //Show the images section. Show the cropped images. This is also where other the images will be appended.
            $('#btn-predict').show(); //Show the predict button now that we have the cropped image
            $('#overlay').hide(200); //Hide the black overlay behind the crop tool. 
            $('#ImageCropContainer').hide(200); //Hide crop tool.
        })
    });





	/**
	* Closes the Crop overlay tool. Event listner. 
	* @param  {Function}
	*/

    $('.close_crop_model').click(function (event) {
        $('#overlay').hide(200); //Hide the black transparent background overlay behind the crop tool. 
        $('#ImageCropContainer').hide(200); //The the crop container and everything inside it.
    });




	/**
	* Clean the string using RegEx. 
	* Replace all characters that are non alphanumeric with blank (except '.' and '_'). Re-perform replace on string, '_' with space.
	* @param  {String}
	* @return {String} 
	*/

    function cleanpredictdata(data) {
        return (data.replace(/[^0-9a-z._]/gi, '')).replace('_', ' ');
    }




	/**
	 * Predict button click event listner. Executes Code when the predict button is clicked 
	 * @param  {function} Pass function name or define a function.
	 * @return {void} 
	*/

    $('#btn-predict').click(function () {

        // Show loading animation [Starts]
        $('#btn-predict').hide(600); //Hide Prediction Button
        $('#btn_container').css("float", "none");
        $('.loader').show(600);      //Show GIF Loader
        $('.block').hide(600);
        $('.menubutton').hide(600);

        // Do a XHR Request and Pass the cropped image to Covid API.
        $.ajax({
            type: 'POST',                                  //HTTP Request Type: POST.
            url: '/predictapi',                            //COVID API Path
            data: { cropped_img: $("#image1").attr("src") }, //Data Payload. The image in string format (Base64).
            cache: false,                                  //Use Cache incase the data payload is the same.
            async: true,                                   //Code continues to be executed If 'false', JS waits on this line until something is returned from the server.


			/**
			* Execute if the XHR (Ajax) Request was a success [200 OK HTTP Response]. 
			* Processes and shows returned images. 
			* @param  {String} Base64Images, Analysis Result and Percentage Data as string sperated with '  ' as delimiter.
			* @return {void} 
			*/

            success: function (data) {
                var combined_arr = data.split("  ");          //Split the string on delimiter. @return {array}
                var b64BoneSupp = combined_arr[0];            //Base64 Image
                var b64Subtracted = combined_arr[1];          //Base64 Image
                var b64Histo = combined_arr[2];           	  //Base64 Image
                var base64Stringimage = combined_arr[3];      //Base64 Image
                var analysis_result = combined_arr[4];        //Analysis Result
                var percentage_data = combined_arr[5];        //Analysis Percentages [Diffrentials] 
                percentage_data = percentage_data.split(" "); //Key and Pair values. Further Split them on space. @return array;

                //Interate over the splited data and clean percnetage data using RegExp
                for (var x = 0; x < percentage_data.length; x++) {
                    percentage_data[x] = cleanpredictdata(percentage_data[x]);
                }

                if (analysis_result == '3') {
                    $('#result').html(' Result: No Viral class/Negative'); //Put text in-between the tags of element with ID: 'result'
                    $('#result').addClass("result_ok");                    //Appends CSS Class to HTML Element #result. CSS Class is for coloring.
                }

                if (analysis_result == '1') {
                    $('#result').html(' Result: Non Covid-Viral Pneumonia/Posititve');
                    $('#result').addClass("result_v_pneumonia");
                }

                if (analysis_result == '2') {
                    $('#result').html(' Result: Bacterial Pneumonia/Posititve');
                    $('#result').addClass("result_b_pneumonia");
                }

                if (analysis_result == '0') {
                    $('#result').html(' Result: Covid-19/Posititve');
                    $('#result').addClass("result_covid");
                }

                //Percentage Data was Key-Value pairs (sorted by pair in descending) and was split with space. 
                //That gives us 4 array elements. We just use key names. Skip the rendering of percnetage values. 
                $("#percent_data").append("<tr><td>Primary Diffrential Diagnosis</td><td>" + percentage_data[0] + "</td></tr>");
                $("#percent_data").append("<tr><td>Secondary Diffrential Diagnosis</td><td>" + percentage_data[2] + "</td></tr>");

                $("#sample_sidebar").hide(); //Hide the right sidebar input instructions because we don't need them after the processing.
                $('#result').show(600); //Make the result element visisble. The Analysis result and color bar. 
                $('#graph-section').show(); //Show the diffrentials.

                //Append the images to the image section.
                $("#image-section").append(`<div><p>Bone suppressed</p><div><img id="image2" src='data:image/png;base64,` + b64BoneSupp + `'></div></div>`);
                $("#image-section").append(`<div><p>Segmentated</p><div><img id="image3" src='data:image/png;base64,` + b64Subtracted + `'></div></div>`);
                $("#image-section").append(`<div><p>GradCAM</p><div><img id="image4" src='data:image/png;base64,` + b64Histo + `'></div></div>`);


                $("#upload-file").prepend(`<span style="font-size: 120%;"><b>Patient ID:` + $("#patient_id").val() + `</b></span>`); //Display the Patient-ID entered earlier.
                $('.loader').hide(600);     //Hide the loader
                $('#btn-reload').show(600); //Show reload button
                $('.menubutton').show(600); //Show menu button.
                //End of Ajax function. 
            },
        });
    });

});

