# Basic Utilities
from __future__ import division, print_function
import sys
import os
import glob
import re
import numpy as np
import base64

# Keras
import keras
from keras.optimizers import Adam
from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.models import load_model, Model
from keras.preprocessing import image
import json
from keras.models import model_from_json
import cv2
import tensorflow as tf
from tensorflow.python.keras.backend import set_session
from keras.layers import Input, Concatenate, Flatten, Lambda

from keras.preprocessing.image import img_to_array, load_img
from keras import backend as K

# Visualization
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from skimage.io import imread
import matplotlib.image as mpimg
from datetime import datetime


# Flask utils
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename

app = Flask(__name__)


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers["Cache-Control"] = "public, max-age=0"
    return r


# Model saved with Keras model.save()
MODEL_PATH = "model.h5"


global sess
global graph
sess = tf.Session()
graph = tf.get_default_graph()
set_session(sess)

# Model Architecture
model = keras.models.Sequential(
    [
        keras.layers.Conv2D(
            32, (3, 3), strides=(1, 1), padding="same", input_shape=(256, 256, 3)
        ),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(64, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(128, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(256, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(512, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        keras.layers.Conv2D(1024, (3, 3), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(1024, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(512, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Conv2D(256, (1, 1), strides=(1, 1), padding="same"),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Flatten(),
        keras.layers.Dense(1024),
        keras.layers.BatchNormalization(),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(512),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(256),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(128),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(64),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dropout(rate=0.5),
        keras.layers.Dense(32),
        keras.layers.BatchNormalization(),
        keras.layers.Activation("relu"),
        keras.layers.Dense(4, activation="softmax"),
    ]
)

# Loss for Classification model
def focal_loss(y_true, y_pred):
    gamma = 2.0
    alpha = 0.25
    # Define epsilon so that the backpropagation will not result in NaN
    # for 0 divisor case
    epsilon = K.epsilon()
    # Add the epsilon to prediction value
    # y_pred = y_pred + epsilon
    # Clip the prediction value
    y_pred = K.clip(y_pred, epsilon, 1.0 - epsilon)
    # Calculate cross entropy
    cross_entropy = -y_true * K.log(y_pred)
    # Calculate weight that consists of  modulating factor and weighting factor
    weight = alpha * y_true * K.pow((1 - y_pred), gamma)
    # Calculate focal loss
    loss = weight * cross_entropy
    # Sum the losses in mini_batch
    loss = K.sum(loss, axis=1)
    return loss


# Compiling and loading model
model.compile(optimizer=Adam(), loss=focal_loss, metrics=["categorical_accuracy"])

model.load_weights("focal3_crop_more_imgs")

# print(model.summary())

json_file = open("model_update.json", "r")
model_json = json_file.read()
json_file.close()
model_prog = model_from_json(model_json)
# load weights into new model
model_prog.load_weights("model_update.h5")
loaded_model = model_prog


# Bone Suppression Model (Suppresses Bones in Chest X-Ray)
bone_model = tf.keras.models.load_model("g_model_w_loss")


def dice_loss(y_true, y_pred):
    numerator = 2 * tf.reduce_sum(y_true * y_pred, axis=-1)
    denominator = tf.reduce_sum(y_true + y_pred, axis=-1)

    return 1 - (numerator + 1) / (denominator + 1)


bone_model.compile(loss=dice_loss, optimizer="adam")


# Segmentation Model (Segments out the lungs)
json_file = open("v_model_darwin_101.json", "r")
model_json = json_file.read()
json_file.close()
vnet_model = model_from_json(model_json)
# load weights into new model
vnet_model.load_weights("v_model_darwin_101.h5")


print("Model loaded. Check http://127.0.0.1:5000/")

# Image Preprocessing
def equalize_light(image, limit=3, grid=(7, 7), gray=False):
    if len(image.shape) == 2:
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        gray = True

    clahe = cv2.createCLAHE(clipLimit=limit, tileGridSize=grid)
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)

    cl = clahe.apply(l)
    limg = cv2.merge((cl, a, b))

    image = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    if gray:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return np.uint8(image)


# Image Preprocessing
def transform_img_fn(path_list):
    out = []
    for img_path in path_list:
        image = cv2.imread(img_path)
        image = cv2.resize(image, (256, 256))
        image = img_to_array(image)
        out.append(image)
    return out


def model_predict(img_path, model):

    # labels = [] # This will contain all the Image labels (0-Covid, 1-Viral Pneumonia, 2-Bacterial Pneumonia, 3-Normal)

    path_list = glob.glob(img_path)  # in use

    image = cv2.resize(cv2.imread(path_list[0]), (256, 256))
    img_copy = image.copy()

    img = transform_img_fn(path_list)

    with graph.as_default():
        set_session(sess)
        data = (
            np.array(img, dtype="float") / 255.0
        )  # This will contain unprocessed images of shape (256, 256, 3)

        # Applying Bone Suppression and saving
        bone_result = bone_model.predict(data)
        x = np.arange(10)
        y = np.arange(10)
        plt.figure(figsize=(30, 30))
        plt.xticks(x, "")
        plt.yticks(y, "")
        plt.imshow(bone_result[0])
        plt.savefig("./static/images/bs-output.jpg", bbox_inches="tight")

        no_bone = cv2.imread(
            "./static/images/bs-output.jpg"
        )  # Using cv2 for img read and superimpose
        no_bone = cv2.resize(no_bone, (256, 256))
        no_bone_copy = no_bone.copy()  # for  the backdrop image

        # Applying Lung Segmentation
        inn = []
        image = cv2.imread(path_list[0])
        image = cv2.resize(image, (256, 256))
        inn.append(image.reshape([256, 256, 3]))
        Y = np.stack(inn, axis=0)
        Y = Y / 255.0
        new_results = vnet_model.predict(Y)
        new_results = new_results.reshape([256, 256])

        i = 0
        kernel = np.ones((25, 25), np.uint8)  # kernel size
        y_ = np.where(new_results > 0.5, 1, 0)  # true mask
        y_ = y_.astype("uint8")
        # dilation = cv2.dilate(y_,kernel,iterations = 1)
        crop = []  # This will contain all the segmented images of shape (256,256,3)
        crop.append(cv2.bitwise_and(bone_result[0], bone_result[0], mask=y_))

        # Lung Segmentation (Image Save)
        x = np.arange(10)
        y = np.arange(10)
        plt.figure(figsize=(30, 30))
        plt.xticks(x, "")
        plt.yticks(y, "")
        plt.imshow(crop[0])
        plt.savefig("./static/images/lungs.jpg", bbox_inches="tight")
        lung_img = cv2.imread("./static/images/lungs.jpg")
        lung_img_copy = lung_img.copy()  # for the backdrop ==0 condition
        lung_img_copy = cv2.resize(lung_img_copy, (256, 256))

        # Backdrop Lung Segmentation
        heatmap = np.mean(0)
        heatmap = np.maximum(heatmap, 0)
        heatmap /= np.max(heatmap)
        heatmap = cv2.resize(heatmap, (lung_img_copy.shape[1], lung_img_copy.shape[0]))
        heatmap = np.uint8(255 * heatmap)
        heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
        hif = 0.4
        superimposed_img_blue = heatmap * hif + lung_img_copy

        # GradCAM plotting on new vnet model + bone suppression

        image = bone_result
        pred_class = model.predict_classes(image)

        preds = model.predict(image)
        argmax = np.argmax(preds[0])

        tot_layers = []
        for layer in model.layers:
            some_layer = layer.get_output_at(0)
            tot_layers.append(some_layer)
        output = tot_layers[
            -1
        ]  # Taking the output of the second last dense layer -3 -20 -16
        last_conv_layer = tot_layers[-25]  # Taking the last convolution layer -13 -27
        output = output[:, argmax]
        grads = K.gradients(output, last_conv_layer)[0]
        pooled_grads = K.mean(grads, axis=(0, 1, 2))
        iterate = K.function([model.input], [pooled_grads, last_conv_layer[0]])
        pooled_grads_value, conv_layer_output_value = iterate([image])  # new_results2
        for i in range(256):
            conv_layer_output_value[:, :, i] *= pooled_grads_value[i]
        heatmap = np.mean(conv_layer_output_value, axis=-1)
        heatmap = np.maximum(heatmap, 0)
        heatmap /= np.max(heatmap)
        img = cv2.imread(path_list[0])
        img = cv2.resize(img, (256, 256))
        heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
        heatmap = np.uint8(255 * heatmap)
        heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
        hif = 0.4
        superimposed_img = heatmap * hif + no_bone

        output = "./static/images/okay2.jpg"
        cv2.imwrite(output, superimposed_img)

        # Sending data to front end
        with open("./static/images/bs-output.jpg", "rb") as img_file:
            b64_string_bs = base64.b64encode(img_file.read()).decode("utf-8")

        with open("./static/images/lungs.jpg", "rb") as img_file:
            b64_string_sub = base64.b64encode(img_file.read()).decode("utf-8")

        with open("./static/images/okay2.jpg", "rb") as img_file:
            b64_string_op = base64.b64encode(img_file.read()).decode("utf-8")

        with open("./static/images/chart.jpg", "rb") as img_file:
            b64_string_chart = base64.b64encode(img_file.read()).decode("utf-8")

        pred_class_str = np.array2string(pred_class)[1]

        pred_list = preds.tolist()
        patho_dict = {
            "covid": pred_list[0][0] * 100,
            "viral_pneumonia": pred_list[0][1] * 100,
            "bacterial_pneumonia": pred_list[0][2] * 100,
            "normal": pred_list[0][3] * 100,
        }
        sort_orders = sorted(patho_dict.items(), key=lambda x: x[1], reverse=True)

        sorted_list = " ".join([str(elem) for elem in sort_orders])
        fin_str = (
            b64_string_bs
            + "  "
            + b64_string_sub
            + "  "
            + b64_string_op
            + "  "
            + b64_string_chart
            + "  "
            + pred_class_str
            + "  "
            + sorted_list
        )

    return fin_str


# RENDERING VIEWS


@app.route("/", methods=["GET", "POST"])
def index():
    error = None
    if request.method == "POST":
        if (
            request.form["username"] != "admin"
            or request.form["password"] != "MUJ@#2011"
        ):
            error = "Invalid Credentials. Please try again."
        else:
            return render_template("predict.html", error=error)
    return render_template("index.html", error=error)


@app.route("/progress", methods=["GET"])
def progress():
    # Progress Page
    return render_template("progress.html")


# HANDLING FORM/INPUT DATA
@app.route("/progressapi", methods=["GET", "POST"])
def upload1():
    if request.method == "POST":
        # Get the file from post request
        imagefile1 = request.files["file1"]
        imagefile2 = request.files["file2"]
        basepath = os.path.dirname(__file__)
        file_path1 = os.path.join(
            basepath, "uploads", secure_filename(imagefile1.filename)
        )
        imagefile1.save(file_path1)
        file_path2 = os.path.join(
            basepath, "uploads", secure_filename(imagefile2.filename)
        )
        imagefile2.save(file_path2)

        with graph.as_default():
            set_session(sess)

            # Taking the output of the Required Layer
            inputA = Input(shape=(256, 256, 3))

            embed_1 = loaded_model.get_layer("conv2d_9")(inputA)
            embed_1 = loaded_model.get_layer("activation_11")(embed_1)
            output_1 = Flatten(name="flatten_A1")(embed_1)
            std_1 = Lambda(lambda x: K.std(x, axis=1), name="S1")(output_1)
            output_1 = Lambda(lambda x: K.mean(x, axis=1), name="L1")(output_1)
            embed_2 = loaded_model.get_layer("max_pooling2d_9")(embed_1)

            embed_3 = loaded_model.get_layer("conv2d_10")(embed_2)
            embed_3 = loaded_model.get_layer("activation_12")(embed_3)
            output_2 = Flatten(name="flatten_B1")(embed_3)
            std_2 = Lambda(lambda x: K.std(x, axis=1), name="S2")(output_2)
            output_2 = Lambda(lambda x: K.mean(x, axis=1), name="L2")(output_2)
            embed_4 = loaded_model.get_layer("max_pooling2d_10")(embed_3)

            embed_5 = loaded_model.get_layer("conv2d_11")(embed_4)
            embed_5 = loaded_model.get_layer("activation_13")(embed_5)
            output_3 = Flatten(name="flatten_C1")(embed_5)
            std_3 = Lambda(lambda x: K.std(x, axis=1), name="S3")(output_3)
            output_3 = Lambda(lambda x: K.mean(x, axis=1), name="L3")(output_3)
            embed_6 = loaded_model.get_layer("max_pooling2d_11")(embed_5)

            embed_7 = loaded_model.get_layer("conv2d_12")(embed_6)
            embed_7 = loaded_model.get_layer("activation_14")(embed_7)
            output_4 = Flatten(name="flatten_D1")(embed_7)
            std_4 = Lambda(lambda x: K.std(x, axis=1), name="S4")(output_4)
            output_4 = Lambda(lambda x: K.mean(x, axis=1), name="L4")(output_4)
            embed_8 = loaded_model.get_layer("max_pooling2d_12")(embed_7)

            embed_9 = loaded_model.get_layer("flatten_3")(embed_8)
            embed_9 = loaded_model.get_layer("dense_11")(embed_9)
            std_5 = Lambda(lambda x: K.std(x, axis=1), name="S7")(embed_9)
            output_7 = Lambda(lambda x: K.mean(x, axis=1), name="L7")(embed_9)

            embed_10 = loaded_model.get_layer("dense_12")(embed_9)
            std_6 = Lambda(lambda x: K.std(x, axis=1), name="S8")(embed_10)
            output_8 = Lambda(lambda x: K.mean(x, axis=1), name="L8")(embed_10)

            embed_11 = loaded_model.get_layer("dense_13")(embed_10)
            std_7 = Lambda(lambda x: K.std(x, axis=1), name="S9")(embed_11)
            output_9 = Lambda(lambda x: K.mean(x, axis=1), name="L9")(embed_11)

            embed_12 = loaded_model.get_layer("dense_14")(embed_11)
            std_8 = Lambda(lambda x: K.std(x, axis=1), name="S10")(embed_12)
            output_10 = Lambda(lambda x: K.mean(x, axis=1), name="L10")(embed_12)

            embed_13 = loaded_model.get_layer("activation_15")(embed_12)
            embed_14 = loaded_model.get_layer("dense_15")(embed_13)
            output_10 = Lambda(lambda x: K.mean(x, axis=1), name="L10")(embed_14)

            embed_20 = [
                output_1,
                output_2,
                output_3,
                output_4,
                output_7,
                output_8,
                output_9,
                output_10,
                std_1,
                std_2,
                std_3,
                std_4,
                std_5,
                std_6,
                std_7,
                std_8,
            ]
            embedd = Model(inputs=[inputA], outputs=embed_20, name="embedd")

            progress_img1 = cv2.imread(file_path1)
            progress_img2 = cv2.imread(file_path2)

            progress_img1 = cv2.resize(progress_img1, (256, 256)) / 255.0
            progress_img2 = cv2.resize(progress_img2, (256, 256)) / 255.0

            progress_img1 = progress_img1.reshape([1, 256, 256, 3])
            progress_img2 = progress_img2.reshape([1, 256, 256, 3])

            embedding_1 = embedd.predict(progress_img1)  # 1,256
            embedding_2 = embedd.predict(progress_img2)  # 1,256

            # Subtracting the mean of the output layers
            diff = []
            change = []
            tolerance_1 = []
            tolerance_2 = []
            mean_1 = []
            mean_2 = []
            for i in range(8):
                mean_1.append(embedding_1[i][0])
                mean_2.append(embedding_2[i][0])
                diff.append(embedding_1[i][0] - embedding_2[i][0])
                change.append(
                    (
                        (embedding_1[i][0] - embedding_2[i][0]) ** 2
                        / embedding_1[i][0] ** 2
                    )
                    ** 0.5
                )
            for i in range(8, 16):
                tolerance_1.append(embedding_1[i][0])
                tolerance_2.append(embedding_2[i][0])
            changes = sum(change) / 10
            count_neg = 0
            count_pos = 0
            diff3 = []
            for i in range(8):
                if diff[i] > 0:
                    diff3.append(1 * change[i])
                    count_pos += 1
                elif diff[i] < 0:
                    diff3.append(-1 * change[i])
                    count_neg += 1
                else:
                    diff3.append(change[i])
            neg = 0
            pos = 0
            for i in diff3:
                if i < 0:
                    neg += np.absolute(i)
                else:
                    pos += np.absolute(i)
            if sum(diff) == 0:
                return "No Change in Viral Infection "
            else:
                if (
                    pos * (count_pos / (count_pos + count_neg))
                    - neg * (count_neg / (count_pos + count_neg))
                    > 0
                ):
                    return (
                        "Change in Viral Infection reduced by -"
                        + str(round(changes * 100, 2))
                        + " ±5%"
                    )
                elif (
                    pos * (count_pos / (count_pos + count_neg))
                    - neg * (count_neg / (count_pos + count_neg))
                    < 0
                ):
                    return (
                        "Change in Viral Infection increased by +"
                        + str(round(changes * 100, 2))
                        + " ±5%"
                    )
                else:
                    return (
                        "No Change in Viral Infection "
                        + str(round(changes * 100, 2))
                        + " ±5%"
                    )


@app.route("/predictapi", methods=["GET", "POST"])
def upload2():
    if request.method == "POST":
        # Get the file from post request
        b64_img = request.form["cropped_img"].replace(" ", "+")
        imgdata = base64.b64decode(b64_img[22:])
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        filename = "./uploads/" + str(timestamp) + ".png"
        with open(filename, "wb") as f:
            f.write(imgdata)
        pred_class = model_predict(filename, model)
        result = str(pred_class)
        return result

    return None


if __name__ == "__main__":
    app.run(debug=True)

